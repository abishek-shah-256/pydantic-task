from pydantic import BaseModel, Field, field_validator
 
from datetime import date

class Company(BaseModel):
    company_name: str
    contact_no: int
    address: str

class Employee(BaseModel):
    name : str
    emp_id: int
    salary: float
    joined_date: date
    company_detail: Company


    # we can use @model_serializer('name') to only changed name instead of whole changes
    @field_validator('name')
    @classmethod
    def to_uppercase(cls, value) -> str:
        return value.upper()
    

    @field_validator('salary')
    @classmethod
    def for_salary(cls, value):
        return '{:.2f}'.format(value)
    
    @field_validator('joined_date')
    @classmethod
    def format_date(cls, value):
        return value.strftime('%Y-%m-%d')
    
    @field_validator('company_detail')
    @classmethod
    def capitalize_company_name(cls, value):
        return Company(company_name=value.company_name.capitalize(), contact_no=value.contact_no, address= value.address)


obj = Employee(name='Sangharsha', emp_id=10245, salary=1500, joined_date=date(2023, 8, 14), company_detail={'company_name':'codehimalaya','contact_no':555,'address':'Kupondole'})
print(obj.model_dump_json(indent=2))

